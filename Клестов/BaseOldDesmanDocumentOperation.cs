﻿using ShtrihM.Starling.Shared.Common.Csv;

namespace ShtrihM.Starling.Shared.Common.DesmanDocuments.Operations
{
    /// <summary>
    /// Операция (старый формат CSV).
    /// </summary>
    public abstract class BaseOldDesmanDocumentOperation : BaseDesmanDocumentOperation
    {
        #region Public Class OldDesmanDocumentOperationAttribute
        /// <summary>
        /// Атрибут (старый формат CSV).
        /// </summary>
        public sealed class OldDesmanDocumentOperationAttribute
        {
            /// <summary>
            /// Идентификатор значения.
            /// </summary>
            public long Id;

            /// <summary>
            /// Имя значения.
            /// Может быть <c>null</c>.
            /// </summary>
            public string Name;
        }
        #endregion

        #region Public Class OldDesmanDocumentOperationAttributes
        /// <summary>
        /// Обязательные атрибуты (старый формат CSV).
        /// </summary>
        public sealed class OldDesmanDocumentOperationAttributes
        {
            /// <summary>
            /// Тип смарт-карты.
            /// </summary>
            public int SmartCardType;

            /// <summary>
            /// Подтип смарт-карты.
            /// </summary>
            public int SmartCardSubType;

            /// <summary>
            /// Номер рейса.
            /// </summary>
            public int TripNumber;

            /// <summary>
            /// ID маршрута.
            /// Не может быть <c>null</c>.
            /// </summary>
            public OldDesmanDocumentOperationAttribute RouteCode;

            /// <summary>
            /// Номер выхода (наряда) расписания.
            /// </summary>
            public int WayoutNumber { get; set; }

            /// <summary>
            /// Гаражный код (код ТС).
            /// Не может быть <c>null</c>.
            /// </summary>
            public OldDesmanDocumentOperationAttribute GarageCode;

            /// <summary>
            /// Табельный номер кондуктора.
            /// Не может быть <c>null</c>.
            /// </summary>
            public OldDesmanDocumentOperationAttribute ConductorCode;

            /// <summary>
            /// Государственный номер машины.
            /// Максимально 50 символов.
            /// Не может быть <c>null</c>.
            /// </summary>
            public string CarPublicNumber;

            /// <summary>
            /// Гаражный номер.
            /// Максимально 50 символов.
            /// Не может быть <c>null</c>.
            /// </summary>
            public string VehicleNumber { get; set; }

            /// <summary>
            /// Тип оборудования.
            /// </summary>
            public TypeOfEquipment EquipmentType { get; set; }

            /// <summary>
            /// Счетчик поездок наземного транспорта.
            /// </summary>
            public int CounterNgpt { get; set; }

            /// <summary>
            /// Счетчик поездок метро.
            /// </summary>
            public int CounterMetro { get; set; }

            /// <summary>
            /// Код зоны начала поездки.
            /// Может быть <c>null</c>.
            /// </summary>
            public OldDesmanDocumentOperationAttribute BeginZone;

            /// <summary>
            /// Код зоны окончания поездки.
            /// Может быть <c>null</c>.
            /// </summary>
            public OldDesmanDocumentOperationAttribute EndZone;

            /// <summary>
            /// Cтоимость по зоналке фактически расчитания на ТС.
            /// Может быть <c>null</c>.
            /// </summary>
            public uint? ZoneAmount { get; set; }

            /// <summary>
            /// Версия ПО.
            /// Максимально 50 символов.
            /// Может быть <c>null</c>.
            /// </summary>
            public string SoftwareVersion { get; set; }

            /// <summary>
            /// Код ошибки оборудования.
            /// Может быть <c>null</c>.
            /// </summary>
            public int? ErrorCode { get; set; }
        }
        #endregion

        protected BaseOldDesmanDocumentOperation(ushort documentTypeId)
            : base(documentTypeId)
        {
        }

        /// <summary>
        /// Идентификатор организации в Системе.
        /// </summary>
        public long OrgaizationId;

        /// <summary>
        /// Аппаратный идентификатор устройства.
        /// </summary>
        public byte[] DeviceNativeId;

        /// <summary>
        /// Идентификатор устройства в Системе.
        /// </summary>
        public long DeviceId;

        /// <summary>
        /// Аппаратный идентификатор смарт-карты.
        /// </summary>
        public byte[] SmartCardNativeId;

        /// <summary>
        /// Идентификатор смарт-карты в Системе.
        /// </summary>
        public long SmartCardId;

        /// <summary>
        /// Обязательные атрибуты.
        /// </summary>
        public OldDesmanDocumentOperationAttributes Attributes;
    }
}