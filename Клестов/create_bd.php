<?php

require_once 'dsn.php';
$dbconn = pg_connect(DSN);

/*
Запрос на добавление записи в агегаты
INSERT INTO "dm1" ("organization_id", "ticket_type", "date", "ride_count", "money") VALUES
(
    (SELECT floor(random() * 10 + 1)::int),  
    (SELECT floor(random() * 10 + 1)::int),  
    now() - (SELECT floor(random() * 10 + 0)::int) * '1 day'::interval,   
    (SELECT floor(random() * 1000 + 1)::int),
    (SELECT floor(random() * 1000000 + 100)::int)
);
*/

// Полезные запросы:
// Удаление схемы совсеми таблицами
// DROP SCHEMA "bin" CASCADE;
// CREATE SCHEMA IF NOT EXISTS "20181011"
// CREATE SEQUENCE IF NOT EXISTS uuid_id_seq
// Заголовки

/*


Desman ActualDocument:
'{
  "OrgaizationId": 9069370780812634533,
  "DeviceNativeId": "tpfV7wcPZwg=",
  "DeviceId": -2146630587949152475,
  "SmartCardNativeId": "p6QXXz7CzqA=",
  "SmartCardId": -363781109476306619,
  "Attributes": {
    "SmartCardType": 600698338,
    "SmartCardSubType": 356925585,
    "TripNumber": -1057928069,
    "RouteCode": 546547485556750621,
    "GarageCode": -1675652127112083459,
    "ConductorCode": {
      "Id": -2411748752952241517,
      "Name": "K^1$=,b0#^cEuM5."
    },
    "CarPublicNumber": "^yH\'nmMOoAM<#3C6",
    "BeginZone": -8243273619547503884,
    "EndZone": -5481249900685248121,
    "WayoutNumber": 968895039,
    "VehicleNumber": "8c jTe,02UY q55\\\\",
    "EquipmentType": 1,
    "CounterNgpt": -494538048,
    "CounterMetro": -1849485820,
    "ZoneAmount": 3837042560,
    "SoftwareVersion": "GI2 L@>HB[\\\\k2khO",
    "ErrorCode": -1573615019
  },
  "DocumentTypeId": 1, // А вот тут есть вопрос - если другой тип операции - то эта запись выглядит по другому!!!
  "RegisterDate": "2000-01-01T00:01:45.76",
  "IssuerDate": "2000-01-01T00:03:18.55",
  "OperationKey": "0add6f31-e00d-482f-84f8-b24ab45ca558",
  "Amount": 3409957248,
  "SmartCardBalance": 2619669289,
  "NeroTransactionId": null,
  "NeroServerId": null
}'


---------------------------------
Desman: HTTP

, RequestUri: 'http://localhost:56646/bufferblock', Version: 1.1, Content: System.Web.Http.WebHost.HttpControllerHandler+LazyStreamContent, Headers:
{
  Expect: 100-continue
  Host: localhost:56646
  ShtrihM-Server: Desman
  ShtrihM-Content: Operation
  ApplicationInsights-RequestTrackingTelemetryModule-RootRequest-Id: 8579350d-eea6-411d-8c94-c2b0aeecbed4
  Content-Length: 2074
  Content-Type: multipart/form-data; boundary="c1775694-6074-4b48-a22f-caaeedd88e7f"
}--c1775694-6074-4b48-a22f-caaeedd88e7f
Content-Type: text/plain; charset=utf-8
Content-Disposition: form-data; name=Operation

{"ServerId":"bf93c687-77c1-4b63-b27b-c9e05ccbf6bc","Id":1664129092,"Type":1,"Category":"Operation"}
--c1775694-6074-4b48-a22f-caaeedd88e7f
Content-Type: text/plain; charset=us-ascii
Content-Disposition: form-data; name=OriginalDocument

Ah+LCAAAAAAABADz6papE6/Q8gt4vG1u/bNtxwGKbtlzEAAAAA==
--c1775694-6074-4b48-a22f-caaeedd88e7f
Content-Type: text/plain; charset=utf-8
Content-Disposition: form-data; name=ActualDocument

{"OrgaizationId":5155237619608738526,"DeviceNativeId":"s+0miIQMy4TpXsWx+5k2XA==","DeviceId":-94521706283569634,"SmartCardNativeId":"fAheX+GS5A==","SmartCardId":5673732248506741527,"Attributes":{"SmartCardType":0,"SmartCardSubType":0,"TripNumber":0,"RouteCode":2748397556058530911,"GarageCode":-2116108109723781193,"ConductorCode":null,"CarPublicNumber":"j^!A^\"$B$P$8B6.&","BeginZone":null,"EndZone":null,"WayoutNumber":0,"VehicleNumber":null,"EquipmentType":1,"CounterNgpt":0,"CounterMetro":0,"ZoneAmount":null,"SoftwareVersion":null,"ErrorCode":null},"DocumentTypeId":1,"RegisterDate":"2018-10-11T20:47:19.401265+03:00","IssuerDate":"2018-10-11T20:47:19.401265+03:00","OperationKey":"00095a2f-b2aa-46a9-9dbf-f2ca8f873964","Amount":16630980,"SmartCardBalance":12551,"NeroTransactionId":1664129092,"NeroServerId":"bf93c687-77c1-4b63-b27b-c9e05ccbf6bc"}
--c1775694-6074-4b48-a22f-caaeedd88e7f
Content-Type: text/plain; charset=utf-8
Content-Disposition: form-data; name=DocumentEvents

[{"FileName":"<wTeN/$,c+gg@7]e","Line":1080070972,"FacadeVersion":{"Major":0,"Minor":10,"Build":10,"Revision":1,"MajorRevision":0,"MinorRevision":1},"FacadeHostName":null,"EventTypeId":1,"CreateDate":"2000-01-01T00:04:33.76"},{"ServerId":"bf93c687-77c1-4b63-b27b-c9e05ccbf6bc","RegisterId":1664129092,"EventTypeId":2,"CreateDate":"2018-10-11T20:47:20.56"},{"ServerId":"bf93c687-77c1-4b63-b27b-c9e05ccbf6bc","CountUnknowns":0,"EventTypeId":4,"CreateDate":"2018-10-11T00:00:00"}]
--c1775694-6074-4b48-a22f-caaeedd88e7f--


---------------------------------
Nero: HTTP
 ["POST"]=>
  array(8) {
    ["Operation"]=>
    string(109) "{"ServerId":"4345feb7-5d2f-4e2d-ade6-29c777baebb0","Id":1481989690,"Type":2,"Cost":12300,"Category":"Online"}"
    ["Document"]=>
    string(1616) "EQC3/kVDL10tTq3mKcd3uuuw4gcKCw8CKFFAAAAABC4DIoJYgBhCAIAAAAAmFwEAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAMAAAAAAAAAfDeIAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAUAAAAAAAAAAAAAAAAAAAAAAAYAAAAAAAAAAAAAAAAAAAAAAAcAAAAAAAAAfDeIAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAkAAAAAAAAAAAAAAAAAAAAAAAoAAAAAAAAAAAAAAAAAAAAAAAsAAAAAAAAAfDeIAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAA0AAAAAAAAAAAAAAAAAAAAAAA4AAAAAAAAAAAAAAAAAAAAAAA8AAAAAAAAAfDeIAAAAAAAAABAADDAAAPPP//8MMAAAEe4R7hEADDAAAPPP//8MMAAAEe4R7hIA/ACmb0YAAAAMMAD//////xMAAAAAAAAASHeLAAAAAAAAABQAbDxGAcd/AQAAAgAAAABTXxUAAANsPEYAdLXnvgAAAAAAABYAAANsPEYAdLXnvgAAAAAAABcAAAAAAAAAfweIAAAAAAAAABgAAAAAAAAAAAAAAAAAAAAAABkAAAAAAAAAAAAAAAAAAAAAABoAAAAAAAAAAAAAAAAAAAAAABsAAAAAAAAAfDeIAAAAAAAAABwAAAAAAAAAAAAAAAAAAAAAAB0AAAAAAAAAAAAAAAAAAAAAAB4AAAAAAAAAAAAAAAAAAAAAAB8AAAAAAAAAfDeIAAAAAAAAACAADakJF+AlABIGHBIGGwD/ESEAEgYcAAAAAAAAAAAAAAAAniIAAAAAAAAAAAAAAAAAAAAAACMAAAAAAAAAfDeIAAAAAAAAACQAAAAAAP////8AAAAAAP8A/yUAAAAAAP////8AAAAAAP8A/yYA7EFGAAAAAAAAAAAAAAAAACcAAAAAAAAATDeLAAAAAAAAACgAAQAAAAAAAAAAAAAAAAAAACkAAAAAAAAAAAAAAB94bnDlTSoAAAAAAAAAAAAAAAAAAAAAACsAAAAAAAAAeHeIAAAAAAAAACwAAAAAAP////8AAAAAAP8A/y0AAAAAAP////8AAAAAAP8A/y4AAAAAAAAAAAAAAAAAAAAAAC8AAAAAAAAASHeLAAAAAAAAADAAEBwANTtHYQEVEgkeBCMp/zEA//8CMjnyPhU3/////////zIAAAAAAAAAAAAAAAAAAAAAADMAAAAAAAAAeHeIAAAAAAAAADQAAAAAAAAAAAAAAAAAAAAAADUAAAAAAAAAAAAAAAAAAAAAADYAAAAAAAAAAAAAAAAAAAAAADcAAAAAAAAAfDeIAAAAAAAAADgAAAAAAAAAAAAAAAAAAAAAADkAAAAAAAAAAAAAAAAAAAAAADoAAAAAAAAAAAAAAAAAAAAAADsAAAAAAAAAfDeIAAAAAAAAADwAAAAAAAAAAAAAAAAAAAAAAD0AAAAAAAAAAAAAAAAAAAAAAD4AAAAAAAAAAAAAAAAAAAAAAD8AAAAAAAAAfDeIAAAAAAAAAJcBGvrSYpMG2N61kOlJE4V+NuPLuGfC6WbC8YHfmA09"
    ["Begin_OriginalDocument"]=>
    string(24) "oDvsRZng/Eu7P6DLzZnXiw=="
    ["Begin_ActualDocument"]=>
    string(3416) "{"Amount":12300,"Bitmap":{"Blocks":[{"Index":0,"Value":"BC4DIoJYgBhCAIAAAAAmFw=="},{"Index":1,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":2,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":3,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":4,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":5,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":6,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":7,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":8,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":9,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":10,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":11,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":12,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":13,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":14,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":15,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":16,"Value":"AAAAAP////8AAAAAEe4R7g=="},{"Index":17,"Value":"AAAAAP////8AAAAAEe4R7g=="},{"Index":18,"Value":"/ACmb0YAAAAAAAD//////w=="},{"Index":19,"Value":"AAAAAAAASHeLAAAAAAAAAA=="},{"Index":20,"Value":"bDxGAcd/AQAAAgAAAABTXw=="},{"Index":21,"Value":"AANsPEYAdLXnvgAAAAAAAA=="},{"Index":22,"Value":"AANsPEYAdLXnvgAAAAAAAA=="},{"Index":23,"Value":"AAAAAAAAfweIAAAAAAAAAA=="},{"Index":24,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":25,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":26,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":27,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":28,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":29,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":30,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":31,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":32,"Value":"DakJF+AlABIGHBIGGwD/EQ=="},{"Index":33,"Value":"EgYcAAAAAAAAAAAAAAAAng=="},{"Index":34,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":35,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":36,"Value":"AAAAAP////8AAAAAAP8A/w=="},{"Index":37,"Value":"AAAAAP////8AAAAAAP8A/w=="},{"Index":38,"Value":"7EFGAAAAAAAAAAAAAAAAAA=="},{"Index":39,"Value":"AAAAAAAATDeLAAAAAAAAAA=="},{"Index":40,"Value":"AQAAAAAAAAAAAAAAAAAAAA=="},{"Index":41,"Value":"AAAAAAAAAAAAAB94bnDlTQ=="},{"Index":42,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":43,"Value":"AAAAAAAAeHeIAAAAAAAAAA=="},{"Index":44,"Value":"AAAAAP////8AAAAAAP8A/w=="},{"Index":45,"Value":"AAAAAP////8AAAAAAP8A/w=="},{"Index":46,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":47,"Value":"AAAAAAAASHeLAAAAAAAAAA=="},{"Index":48,"Value":"EBwANTtHYQEVEgkeBCMp/w=="},{"Index":49,"Value":"//8CMjnyPhU3/////////w=="},{"Index":50,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":51,"Value":"AAAAAAAAeHeIAAAAAAAAAA=="},{"Index":52,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":53,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":54,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":55,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":56,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":57,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":58,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":59,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="},{"Index":60,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":61,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":62,"Value":"AAAAAAAAAAAAAAAAAAAAAA=="},{"Index":63,"Value":"AAAAAAAAfDeIAAAAAAAAAA=="}]},"SmartCardNativeId":"AQIDBAUGBwg=","SmartCardId":1,"RegisterDate":"2018-10-11T15:02:40.37","IssuerDate":"2018-10-11T15:02:40.37","OperationKey":"d9c21883-3e7f-496a-9523-fac2bb5f6456","OrgaizationId":-9223372036854775808,"DeviceNativeId":"afbKWmvxakevHpNOnWksvQ==","DeviceId":-9223372036854775808,"OperationTypeId":5}"
    ["Begin_DocumentEvents"]=>
    string(413) "[{"PublisherId":"de8ba0be-1988-4ed5-b610-e9eb208a7730","EventTypeId":1,"CreateDate":"2018-10-11T15:02:40.77"},{"DeviceId":123,"PublicKeyId":890,"EventTypeId":3,"CreateDate":"2018-10-11T15:02:40.77"},{"DeviceId":123,"Reason":"Тетовый серве","EventTypeId":4,"CreateDate":"2018-10-11T15:02:40.77"},{"ServerId":"4345feb7-5d2f-4e2d-ade6-29c777baebb0","EventTypeId":2,"CreateDate":"2018-10-11T15:02:40.78"}]"
    ["End_OriginalDocument"]=>
    string(24) "GF6U4SLroUmvZyUZ4n2xpA=="
    ["End_ActualDocument"]=>
    string(304) "{"Cost":12300,"OperationId":1481989690,"RegisterDate":"2018-10-11T15:02:40.86","IssuerDate":"2018-10-11T15:02:40.86","OperationKey":"d9c21883-3e7f-496a-9523-fac2bb5f6456","OrgaizationId":-9223372036854775808,"DeviceNativeId":"afbKWmvxakevHpNOnWksvQ==","DeviceId":-9223372036854775808,"OperationTypeId":6}"
    ["End_DocumentEvents"]=>
    string(413) "[{"PublisherId":"de8ba0be-1988-4ed5-b610-e9eb208a7730","EventTypeId":1,"CreateDate":"2018-10-11T15:02:40.86"},{"DeviceId":123,"PublicKeyId":890,"EventTypeId":3,"CreateDate":"2018-10-11T15:02:40.86"},{"DeviceId":123,"Reason":"Тетовый серве","EventTypeId":4,"CreateDate":"2018-10-11T15:02:40.86"},{"ServerId":"4345feb7-5d2f-4e2d-ade6-29c777baebb0","EventTypeId":2,"CreateDate":"2018-10-11T15:02:40.86"}]"
  }
---------------------------------
*/

// В файлы сохранять только ошибки
// оповещать об ошибках?

//{"Amount": 3409957248, "DeviceId": -2146630587949152475, "Attributes": {"EndZone": -5481249900685248121, "BeginZone": -8243273619547503884, "ErrorCode": -1573615019, "RouteCode": 546547485556750621, "GarageCode": -1675652127112083459, "TripNumber": -1057928069, "ZoneAmount": 3837042560, "CounterNgpt": -494538048, "CounterMetro": -1849485820, "WayoutNumber": 968895039, "ConductorCode": {"Id": -2411748752952241517, "Name": "K^1$=,b0#^cEuM5."}, "EquipmentType": 1, "SmartCardType": 600698338, "VehicleNumber": "8c jTe,02UY q55\\", "CarPublicNumber": "^yH'nmMOoAM<#3C6", "SoftwareVersion": "GI2 L@>HB[\\k2khO", "SmartCardSubType": 356925585}, "IssuerDate": "2000-01-01T00:03:18.55", "SmartCardId": -363781109476306619, "NeroServerId": null, "OperationKey": "0add6f31-e00d-482f-84f8-b24ab45ca558", "RegisterDate": "2000-01-01T00:01:45.76", "OrgaizationId": 9069370780812634533, "DeviceNativeId": "tpfV7wcPZwg=", "DocumentTypeId": 1, "SmartCardBalance": 2619669289, "NeroTransactionId": null, "SmartCardNativeId": "p6QXXz7CzqA="}

$date = date('Ymd');
$queries = [
	// Один день - новая схема
	'CREATE SCHEMA IF NOT EXISTS "'.$date.'"',

	// Созданный record_id из этой таблицы - идентификатор всех записей в других таблицах
	// Он уникален для данной схемы. Один уникальный HTTP запрос
	// Формируется из POST данных $_POST['Operation']['ServerId']/$_POST['Operation']['Id']
	// typeServer = Nero/Desman - тип сервера что прислал запрос

	'CREATE TABLE IF NOT EXISTS "'.$date.'".guids (
	  "record_id" serial NOT NULL,
	  "created_at" timestamp NOT NULL DEFAULT now(),
	  "ServerId" uuid NOT NULL,
	  "OperationId" bigint NULL,
	  "typeServer" character varying(30) NULL,
	  PRIMARY KEY ("record_id"),
	  unique ("ServerId", "OperationId")
	)',


	// В этой таблице храним данные в виде одной строки JSON в которую собираем весь запрос
	'CREATE TABLE IF NOT EXISTS "'.$date.'".store (
	  "record_id" int NOT NULL,
	  "data" jsonb NOT NULL,
	  unique ("record_id"),
	  FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT
	)',

	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanActualDocument" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

		"OrgaizationId" bigint NULL,
		"DeviceNativeId" character varying(255) NULL,
		"DeviceId" bigint NULL,
		"SmartCardNativeId" character varying(255) NULL,
		"SmartCardId" bigint NULL,
		"SmartCardType" bigint NULL,
		"SmartCardSubType" bigint NULL,
		"TripNumber" bigint NULL,
		"RouteCode" bigint NULL,
		"GarageCode" bigint NULL,
		"ConductorCodeId" bigint NULL,
		"ConductorCodeName" character varying(255) NULL,
		"CarPublicNumber" character varying(255) NULL,
		"BeginZone" bigint NULL,
		"EndZone" bigint NULL,
		"WayoutNumber" bigint NULL,
		"VehicleNumber" character varying(255) NULL,
		"EquipmentType" bigint NULL,
		"CounterNgpt" bigint NULL,
		"CounterMetro" bigint NULL,
		"ZoneAmount" bigint NULL,
		"SoftwareVersion" character varying(255) NULL,
		"ErrorCode" bigint NULL,
		"DocumentTypeId" bigint NULL,
		"RegisterDate" timestamp NULL,
		"IssuerDate" timestamp NULL,
		"OperationKey" uuid NOT NULL,
		"Amount" bigint NULL,
		"SmartCardBalance" bigint NULL,
		"NeroTransactionId" bigint NULL,
		"NeroServerId" uuid NULL
	);',

	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanOriginalDocument" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

		data text NOT NULL
	);',


	// Ссылка на запись в таблице ТИПОВЫХ событий. Заполнение всех этих таблиц происходит в рамках одной транзакции
	'CREATE SEQUENCE IF NOT EXISTS "'.$date.'"."DesmanDocumentEvents_LinkId_seq";',

	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanDocumentEvents" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

        "EventTypeId" int NOT NULL,
        "CreateDate" timestamp NULL,
        "LinkId" integer DEFAULT nextval(\'"'.$date.'"."DesmanDocumentEvents_LinkId_seq"\')  NOT NULL,
        unique ("LinkId")
	);',

	// {"ServerId":"bf93c687-77c1-4b63-b27b-c9e05ccbf6bc","Id":1664129092,"Type":1,"Category":"Operation"}
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanOperation" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

		"Type" int NOT NULL,
		"Category" character varying(255) NULL
	);',

/*
[
    {
        "FileName": "<wTeN/$,c+gg@7]e",
        "Line": 1080070972,
        "FacadeVersion": {
            "Major": 0,
            "Minor": 10,
            "Build": 10,
            "Revision": 1,
            "MajorRevision": 0,
            "MinorRevision": 1
        },
        "FacadeHostName": null,
        "EventTypeId": 1,
        "CreateDate": "2000-01-01T00:04:33.76"
    },
    {
        "ServerId": "bf93c687-77c1-4b63-b27b-c9e05ccbf6bc",
        "RegisterId": 1664129092,
        "EventTypeId": 2,
        "CreateDate": "2018-10-11T20:47:20.56"
    },
    {
        "ServerId": "bf93c687-77c1-4b63-b27b-c9e05ccbf6bc",
        "CountUnknowns": 0,
        "EventTypeId": 4,
        "CreateDate": "2018-10-11T00:00:00"
    }
]
*/

	// "EventTypeId": 1,
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanDocumentEvents_t1" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."DesmanDocumentEvents" ("LinkId") ON DELETE RESTRICT,

        "FileName" character varying(255) NULL,
        "Line" bigint NULL,
        "FacadeVersion" jsonb NULL,
        "FacadeHostName" character varying(255) NULL
	);',

	// "EventTypeId": 2,
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanDocumentEvents_t2" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."DesmanDocumentEvents" ("LinkId") ON DELETE RESTRICT,

		"ServerId" uuid NULL,
        "RegisterId" bigint NULL
	);',

	// "EventTypeId": 4, - А где EventTypeId = 3? x_X
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DesmanDocumentEvents_t4" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."DesmanDocumentEvents" ("LinkId") ON DELETE RESTRICT,

		"ServerId" uuid NULL,
        "CountUnknowns" bigint NULL
	);',

	//'INSERT INTO "'.$date.'".guids(record_id, "ServerId", "OperationId", "typeServer") VALUES(1, \'bf93c687-77c1-4b63-b27b-c9e05ccbf6bc\', 1664129092, \'Desman\')',	
	//'INSERT INTO "'.$date.'".store(record_id, data) VALUES(1, \'{"Amount": 3409957248, "DeviceId": -2146630587949152475, "Attributes": {"EndZone": -5481249900685248121, "BeginZone": -8243273619547503884, "ErrorCode": -1573615019, "RouteCode": 546547485556750621, "GarageCode": -1675652127112083459, "TripNumber": -1057928069, "ZoneAmount": 3837042560, "CounterNgpt": -494538048, "CounterMetro": -1849485820, "WayoutNumber": 968895039, "ConductorCode": {"Id": -2411748752952241517, "Name": "K^1$=,b0#^cEuM5."}, "EquipmentType": 1, "SmartCardType": 600698338, "VehicleNumber": "8c jTe,02UY q55\\\\", "CarPublicNumber": "^yH\'\'nmMOoAM<#3C6", "SoftwareVersion": "GI2 L@>HB[\\\\k2khO", "SmartCardSubType": 356925585}, "IssuerDate": "2000-01-01T00:03:18.55", "SmartCardId": -363781109476306619, "NeroServerId": null, "OperationKey": "0add6f31-e00d-482f-84f8-b24ab45ca558", "RegisterDate": "2000-01-01T00:01:45.76", "OrgaizationId": 9069370780812634533, "DeviceNativeId": "tpfV7wcPZwg=", "DocumentTypeId": 1, "SmartCardBalance": 2619669289, "NeroTransactionId": null, "SmartCardNativeId": "p6QXXz7CzqA="}\')',
	//'INSERT INTO "'.$date.'"."DesmanActualDocument" ("created_at", "OrgaizationId", "DeviceNativeId", "DeviceId", "SmartCardNativeId", "SmartCardId", "SmartCardType", "SmartCardSubType", "TripNumber", "RouteCode", "GarageCode", "ConductorCodeId", "ConductorCodeName", "CarPublicNumber", "BeginZone", "EndZone", "WayoutNumber", "VehicleNumber", "EquipmentType", "CounterNgpt", "CounterMetro", "ZoneAmount", "SoftwareVersion", "ErrorCode", "DocumentTypeId", "RegisterDate", "IssuerDate", "OperationKey", "Amount", "SmartCardBalance", "NeroTransactionId", "NeroServerId") VALUES (\'2018-10-11 22:07:18.205649\',	5155237619608739000,	\'s+0miIQMy4TpXsWx+5k2XA==\',	-94521706283569630,	\'fAheX+GS5A==\',	5673732248506742000,	0,	0,	0,	2748397556058531000,	-2116108109723781000,	NULL,	NULL,	\'j^!A^\"$B$P$8B6.&\',	NULL,	NULL,	0,	NULL,	1,	0,	0,	NULL,	NULL,	NULL,	1,	\'2018-10-11 20:47:19.401265\',	\'2018-10-11 20:47:19.401265\',	\'00095a2f-b2aa-46a9-9dbf-f2ca8f873964\',	16630980,	12551,	1664129092,	\'bf93c687-77c1-4b63-b27b-c9e05ccbf6bc\');',


	// Структура под Nero
	// -------------------
	// {"ServerId":"4345feb7-5d2f-4e2d-ade6-29c777baebb0","Id":1481989690,"Type":2,"Cost":12300,"Category":"Online"}
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroOperation" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

		"Type" int NULL,
		"Cost" int NULL,
		"Category" character varying(255) NULL
	);',

	// Сюда пишем Document, Begin_OriginalDocument, End_OriginalDocument
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroDocument" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

		type character varying(30) NULL,
		data text NOT NULL
	);',

	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroBeginActualDocument" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

		"Amount" int NULL,
		"Bitmap" jsonb NULL,
	    "SmartCardNativeId" character varying(255) NULL,
	    "SmartCardId" bigint NULL,
	    "RegisterDate" timestamp NULL,
	    "IssuerDate" timestamp NULL,
	    "OperationKey" uuid NULL,
	    "OrgaizationId" bigint NULL,
	    "DeviceNativeId" character varying(255) NULL,
	    "DeviceId" bigint NULL,
	    "OperationTypeId" int NULL
	);',


	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroEndActualDocument" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

	    "Cost" int NULL,
	    "OperationId" bigint NULL,
	    "RegisterDate" timestamp NULL,
	    "IssuerDate" timestamp NULL,
	    "OperationKey" uuid NULL,
	    "OrgaizationId" bigint NULL,
	    "DeviceNativeId" character varying(255) NULL,
	    "DeviceId" bigint NULL,
	    "OperationTypeId" int NULL
	);',



	// Ссылка на запись в таблице ТИПОВЫХ событий. Заполнение всех этих таблиц происходит в рамках одной транзакции
	'CREATE SEQUENCE IF NOT EXISTS "'.$date.'"."NeroDocumentEvents_LinkId_seq";',

	// isBegin - тип документа -  begin/end
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroDocumentEvents" (
		"record_id" int NOT NULL,
		unique ("record_id"),
		FOREIGN KEY ("record_id") REFERENCES "'.$date.'"."guids" ("record_id") ON DELETE RESTRICT,

        "EventTypeId" int NOT NULL,
        "CreateDate" timestamp NULL,
        "isBegin" bool NULL,
        "LinkId" integer DEFAULT nextval(\'"'.$date.'"."NeroDocumentEvents_LinkId_seq"\')  NOT NULL,
        unique ("LinkId")
	);',

	// "EventTypeId": 1,
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroDocumentEvents_t1" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."NeroDocumentEvents" ("LinkId") ON DELETE RESTRICT,

        "PublisherId" uuid NULL
	);',

	// "EventTypeId": 2,
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroDocumentEvents_t2" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."NeroDocumentEvents" ("LinkId") ON DELETE RESTRICT,

		"DeviceId" bigint NULL,
        "PublicKeyId" bigint NULL
	);',

	// "EventTypeId": 3,
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroDocumentEvents_t3" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."NeroDocumentEvents" ("LinkId") ON DELETE RESTRICT,

        "DeviceId" bigint NULL,
        "Reason" character varying(255) NULL
	);',

	// "EventTypeId": 4,
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."NeroDocumentEvents_t4" (
		"LinkId" int NOT NULL,
		unique ("LinkId"),
		FOREIGN KEY ("LinkId") REFERENCES "'.$date.'"."NeroDocumentEvents" ("LinkId") ON DELETE RESTRICT,

        "ServerId" uuid NULL
	);',

	// В конце дня надо для 1С сделать агрегаты. Эти агрегаты должны делаться если все демоны отчитались что день закрыт
	'CREATE TABLE IF NOT EXISTS "'.$date.'"."DaemonReport" (
        "ServerId" uuid NOT NULL,
        complete_day boolean DEFAULT \'f\' 
	);',	
];

pg_query($dbconn, 'START TRANSACTION;');
foreach ($queries as $query) {
	pg_query($dbconn, $query);
}
pg_query($dbconn, 'COMMIT;');
