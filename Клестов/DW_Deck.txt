Событие смарт-карты №1.
EventId = 1 (Создание смарт-карты)
Схема [SmartCardCreatedEvent] :
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "SmartCardCreatedEvent",
  "type": "object",
  "description": "Создание смарт-карты",
  "additionalProperties": false,
  "required": [
    "EventId",
    "Id",
    "Date",
    "ChronologicalId",
    "NativeId",
    "DeckSecurityPrincipal",
    "IssueDate",
    "PrintId",
    "Form"
  ],
  "properties": {
    "EventId": {
      "type": "integer",
      "description": "Тип документа",
      "format": "byte"
    },
    "Id": {
      "type": "integer",
      "description": "Системный ID смарт-карты",
      "format": "int64"
    },
    "Date": {
      "type": "string",
      "description": "Дата события",
      "format": "date-time"
    },
    "ChronologicalId": {
      "type": "integer",
      "description": "Хронологический ID события",
      "format": "int64"
    },
    "NativeId": {
      "type": "string",
      "description": "Аппаратный номер смарт-карты",
      "format": "byte",
      "maxLength": 255
    },
    "DeckSecurityPrincipal": {
      "description": "Principal",
      "$ref": "#/definitions/DeckSecurityPrincipal"
    },
    "IssueDate": {
      "type": "string",
      "description": "Дата выпуска эмиссионным центром",
      "format": "date-time"
    },
    "PrintId": {
      "type": "string",
      "description": "Печатный номер",
      "maxLength": 26
    },
    "Form": {
      "type": "integer",
      "description": "Тип носителя"
    },
    "Tag": {
      "type": [
        "null",
        "string"
      ],
      "description": "Служебная информация о смарт-карте. Может быть Null"
    }
  },
  "definitions": {
    "DeckSecurityPrincipal": {
      "type": "object",
      "description": "Principal выполняющий манипуляцию на сервере Deck",
      "additionalProperties": false,
      "required": [
        "PrincipalType"
      ],
      "properties": {
        "PrincipalType": {
          "description": "Тип учетной записи",
          "$ref": "#/definitions/DeckSecurityPrincipalType"
        },
        "Sid": {
          "type": [
            "null",
            "string"
          ],
          "description": "Идентификатор учетной записи"
        }
      }
    },
    "DeckSecurityPrincipalType": {
      "type": "integer",
      "description": "Тип учетной записи.",
      "x-enumNames": [
        "System",
        "RsLogin"
      ],
      "enum": [
        1,
        2
      ]
    }
  }
}
Пример:
{
  "IssueDate": "2000-01-01T00:00:14.73",
  "PrintId": "7nccA9kgFwA. _p,",
  "Form": 25618,
  "Tag": "l^J=y:5?dFP7F.EX",
  "EventId": 1,
  "Id": 2994888038958054174,
  "Date": "2000-01-01T00:00:59.18",
  "ChronologicalId": -3888383872921311542,
  "NativeId": "xsjPdnI72uf3ZYdZ2si3Jg==",
  "DeckSecurityPrincipal": {
    "PrincipalType": 1,
    "Sid": "ka`5Mq2ig1NunN^\\"
  }
}
------------------------------------------------------------------------------------------
Событие смарт-карты №2.
EventId = 2 (Активация смарт-карты)
Схема [SmartCardActivatedEvent] :
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "SmartCardActivatedEvent",
  "type": "object",
  "description": "Активация смарт-карты",
  "additionalProperties": false,
  "required": [
    "EventId",
    "Id",
    "Date",
    "ChronologicalId",
    "NativeId",
    "DeckSecurityPrincipal"
  ],
  "properties": {
    "EventId": {
      "type": "integer",
      "description": "Тип документа",
      "format": "byte"
    },
    "Id": {
      "type": "integer",
      "description": "Системный ID смарт-карты",
      "format": "int64"
    },
    "Date": {
      "type": "string",
      "description": "Дата события",
      "format": "date-time"
    },
    "ChronologicalId": {
      "type": "integer",
      "description": "Хронологический ID события",
      "format": "int64"
    },
    "NativeId": {
      "type": "string",
      "description": "Аппаратный номер смарт-карты",
      "format": "byte",
      "maxLength": 255
    },
    "DeckSecurityPrincipal": {
      "description": "Principal",
      "$ref": "#/definitions/DeckSecurityPrincipal"
    }
  },
  "definitions": {
    "DeckSecurityPrincipal": {
      "type": "object",
      "description": "Principal выполняющий манипуляцию на сервере Deck",
      "additionalProperties": false,
      "required": [
        "PrincipalType"
      ],
      "properties": {
        "PrincipalType": {
          "description": "Тип учетной записи",
          "$ref": "#/definitions/DeckSecurityPrincipalType"
        },
        "Sid": {
          "type": [
            "null",
            "string"
          ],
          "description": "Идентификатор учетной записи"
        }
      }
    },
    "DeckSecurityPrincipalType": {
      "type": "integer",
      "description": "Тип учетной записи.",
      "x-enumNames": [
        "System",
        "RsLogin"
      ],
      "enum": [
        1,
        2
      ]
    }
  }
}
Пример:
{
  "EventId": 2,
  "Id": 2274974990863793102,
  "Date": "2000-01-01T00:02:22.67",
  "ChronologicalId": 1742274044402682093,
  "NativeId": "K1yv3MO/AVH5O8nuCKwLTA==",
  "DeckSecurityPrincipal": {
    "PrincipalType": 2,
    "Sid": "/j5N*1iVzK$I\"{DR"
  }
}
------------------------------------------------------------------------------------------
Событие смарт-карты №3.
EventId = 3 (Установка блокировки на ячейку смарт-карты)
Схема [SmartCardCellBlockEvent] :
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "SmartCardCellBlockEvent",
  "type": "object",
  "description": "Установка блокировки на ячейку смарт-карты",
  "additionalProperties": false,
  "required": [
    "EventId",
    "Id",
    "Date",
    "ChronologicalId",
    "NativeId",
    "DeckSecurityPrincipal",
    "Comment",
    "BlockState",
    "CellBlockStateReasone",
    "CellBlockStartDate"
  ],
  "properties": {
    "EventId": {
      "type": "integer",
      "description": "Тип документа",
      "format": "byte"
    },
    "Id": {
      "type": "integer",
      "description": "Системный ID смарт-карты",
      "format": "int64"
    },
    "Date": {
      "type": "string",
      "description": "Дата события",
      "format": "date-time"
    },
    "ChronologicalId": {
      "type": "integer",
      "description": "Хронологический ID события",
      "format": "int64"
    },
    "NativeId": {
      "type": "string",
      "description": "Аппаратный номер смарт-карты",
      "format": "byte",
      "maxLength": 255
    },
    "DeckSecurityPrincipal": {
      "description": "Principal",
      "$ref": "#/definitions/DeckSecurityPrincipal"
    },
    "Comment": {
      "type": "string",
      "description": "Комментаний",
      "maxLength": 2048
    },
    "BlockState": {
      "description": "Код статуса блокировки сводный по всем ячейкам на дату создания события",
      "$ref": "#/definitions/SmartCardBlockStateCode"
    },
    "CellNumber": {
      "type": "integer",
      "description": "Номер ячейки",
      "format": "int32"
    },
    "CellBlockStateReasone": {
      "type": "integer",
      "description": "Код статуса блокировки",
      "format": "int32"
    },
    "CellBlockStartDate": {
      "type": "string",
      "description": "Дата начала блокирования ячейки",
      "format": "date-time"
    },
    "CellBlockReleaseDate": {
      "type": [
        "null",
        "string"
      ],
      "description": "Дата окончания блокирования ячейки",
      "format": "date-time"
    },
    "BlockChangeRejected": {
      "type": "boolean",
      "description": "Было ли успешным изменение состояние ячейки"
    },
    "BlockChangeRejectedReason": {
      "description": "Причина отказа в смене кода блокировки или успех",
      "$ref": "#/definitions/BlockChangeRejectedReason"
    },
    "BlockChangeRejectedReasonComment": {
      "type": [
        "null",
        "string"
      ],
      "description": "Описание причины в смене кода блокировки или успех"
    }
  },
  "definitions": {
    "DeckSecurityPrincipal": {
      "type": "object",
      "description": "Principal выполняющий манипуляцию на сервере Deck",
      "additionalProperties": false,
      "required": [
        "PrincipalType"
      ],
      "properties": {
        "PrincipalType": {
          "description": "Тип учетной записи",
          "$ref": "#/definitions/DeckSecurityPrincipalType"
        },
        "Sid": {
          "type": [
            "null",
            "string"
          ],
          "description": "Идентификатор учетной записи"
        }
      }
    },
    "DeckSecurityPrincipalType": {
      "type": "integer",
      "description": "Тип учетной записи.",
      "x-enumNames": [
        "System",
        "RsLogin"
      ],
      "enum": [
        1,
        2
      ]
    },
    "SmartCardBlockStateCode": {
      "type": "integer",
      "description": "Код цвета смарт-карты.",
      "x-enumNames": [
        "White",
        "Black"
      ],
      "enum": [
        1,
        2
      ]
    },
    "BlockChangeRejectedReason": {
      "type": "integer",
      "description": "",
      "x-enumNames": [
        "Success",
        "OldTask",
        "RejectedByCellPolicy"
      ],
      "enum": [
        1,
        2,
        3
      ]
    }
  }
}
Пример:
{
  "Comment": "Xl]8rbkoBPU;ZDhp",
  "BlockState": 1,
  "CellNumber": 943016140,
  "CellBlockStateReasone": -896111342,
  "CellBlockStartDate": "2000-01-01T00:06:05.67",
  "CellBlockReleaseDate": null,
  "BlockChangeRejected": true,
  "BlockChangeRejectedReason": 2,
  "BlockChangeRejectedReasonComment": "#,8f3KzK~E+Y[M{V",
  "EventId": 3,
  "Id": 6057106615208962952,
  "Date": "2000-01-01T00:03:33.08",
  "ChronologicalId": 8503035656153005466,
  "NativeId": "qhWjNOQ1kZdN1jxPNaJt7g==",
  "DeckSecurityPrincipal": {
    "PrincipalType": 2,
    "Sid": "]@=?5^[pWOF6.-W5"
  }
}
------------------------------------------------------------------------------------------
Событие смарт-карты №4.
EventId = 4 (Снятие блокировки с ячейки смарт-карты)
Схема [SmartCardCellUnBlockEvent] :
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "SmartCardCellUnBlockEvent",
  "type": "object",
  "description": "Снятие блокировки ячейки смарт-карты",
  "additionalProperties": false,
  "required": [
    "EventId",
    "Id",
    "Date",
    "ChronologicalId",
    "NativeId",
    "DeckSecurityPrincipal",
    "Comment",
    "BlockState"
  ],
  "properties": {
    "EventId": {
      "type": "integer",
      "description": "Тип документа",
      "format": "byte"
    },
    "Id": {
      "type": "integer",
      "description": "Системный ID смарт-карты",
      "format": "int64"
    },
    "Date": {
      "type": "string",
      "description": "Дата события",
      "format": "date-time"
    },
    "ChronologicalId": {
      "type": "integer",
      "description": "Хронологический ID события",
      "format": "int64"
    },
    "NativeId": {
      "type": "string",
      "description": "Аппаратный номер смарт-карты",
      "format": "byte",
      "maxLength": 255
    },
    "DeckSecurityPrincipal": {
      "description": "Principal",
      "$ref": "#/definitions/DeckSecurityPrincipal"
    },
    "Comment": {
      "type": "string",
      "description": "Комментаний",
      "maxLength": 2048
    },
    "BlockState": {
      "description": "Код статуса блокировки смарт-карты сводный по всем ячейкам на дату создания события",
      "$ref": "#/definitions/SmartCardBlockStateCode"
    },
    "CellNumber": {
      "type": "integer",
      "description": "Номер ячейки",
      "format": "int32"
    },
    "CellBlockStateReasone": {
      "type": [
        "integer",
        "null"
      ],
      "description": "Код статуса блокировки на момент даты начала блокирования",
      "format": "int32"
    },
    "CellBlockStartDate": {
      "type": [
        "null",
        "string"
      ],
      "description": "Дата начала блокирования ячейки",
      "format": "date-time"
    },
    "CellBlockReleaseDate": {
      "type": [
        "null",
        "string"
      ],
      "description": "Дата окончания блокирования ячейки",
      "format": "date-time"
    },
    "BlockChangeRejected": {
      "type": "boolean",
      "description": "Было ли успешным изменение состояние ячейки"
    },
    "BlockChangeRejectedReason": {
      "description": "Причина отказа в смене кода блокировки или успех",
      "$ref": "#/definitions/BlockChangeRejectedReason"
    },
    "BlockChangeRejectedReasonComment": {
      "type": [
        "null",
        "string"
      ],
      "description": "Описание причины в смене кода блокировки или успех"
    }
  },
  "definitions": {
    "DeckSecurityPrincipal": {
      "type": "object",
      "description": "Principal выполняющий манипуляцию на сервере Deck",
      "additionalProperties": false,
      "required": [
        "PrincipalType"
      ],
      "properties": {
        "PrincipalType": {
          "description": "Тип учетной записи",
          "$ref": "#/definitions/DeckSecurityPrincipalType"
        },
        "Sid": {
          "type": [
            "null",
            "string"
          ],
          "description": "Идентификатор учетной записи"
        }
      }
    },
    "DeckSecurityPrincipalType": {
      "type": "integer",
      "description": "Тип учетной записи.",
      "x-enumNames": [
        "System",
        "RsLogin"
      ],
      "enum": [
        1,
        2
      ]
    },
    "SmartCardBlockStateCode": {
      "type": "integer",
      "description": "Код цвета смарт-карты.",
      "x-enumNames": [
        "White",
        "Black"
      ],
      "enum": [
        1,
        2
      ]
    },
    "BlockChangeRejectedReason": {
      "type": "integer",
      "description": "",
      "x-enumNames": [
        "Success",
        "OldTask",
        "RejectedByCellPolicy"
      ],
      "enum": [
        1,
        2,
        3
      ]
    }
  }
}
Пример:
{
  "Comment": "MJ`Tndr&KW|+Hok(",
  "BlockState": 2,
  "CellNumber": -1046183779,
  "CellBlockStateReasone": null,
  "CellBlockStartDate": "2000-01-01T00:03:29.22",
  "CellBlockReleaseDate": "2000-01-01T00:00:50.95",
  "BlockChangeRejected": false,
  "BlockChangeRejectedReason": 3,
  "BlockChangeRejectedReasonComment": "$dnx/xL6r]`OGfb>",
  "EventId": 4,
  "Id": 8603122932710265243,
  "Date": "2000-01-01T00:03:03.74",
  "ChronologicalId": 1685154281921448109,
  "NativeId": "luRjIpocwhQgyuldPdTUDQ==",
  "DeckSecurityPrincipal": {
    "PrincipalType": 2,
    "Sid": "|^c0OF&<GGQAa6x{"
  }
}
------------------------------------------------------------------------------------------
