﻿using System;
using Newtonsoft.Json;
using ShtrihM.Starling.Shared.Common.Json;

namespace ShtrihM.Starling.Shared.Common.DesmanDocuments.Operations
{
    [JsonConverter(typeof(JsonConverterCustom<BaseDesmanDocumentOperation>))]
    [JsonBaseType(nameof(DocumentTypeId))]
    public abstract class BaseDesmanDocumentOperation
    {
        protected BaseDesmanDocumentOperation(ushort documentTypeId)
        {
            DocumentTypeId = documentTypeId;
        }

        /// <summary>
        /// Тип документа (<see cref="WellknownDesmanDocumentOperations"/>).
        /// </summary>
        public readonly ushort DocumentTypeId;

        /// <summary>
        /// Дата первичной регистрации документа в Системе.
        /// </summary>
        public DateTime RegisterDate;

        /// <summary>
        /// Дата создания документа автором.
        /// </summary>
        public DateTime IssuerDate;

        /// <summary>
        /// Уникальный ключ операции.
        /// </summary>
        public Guid OperationKey;
    }
}
