﻿using System;
using ShtrihM.Starling.Shared.Common.Json;

namespace ShtrihM.Starling.Shared.Common.DesmanDocuments.Operations
{
    /// <summary>
    /// Операция (старый формат CSV) - Поездка по электронному кошельку.
    /// </summary>
    [JsonTypeId(WellknownDesmanDocumentOperations.EWalletDecrement)]
    public sealed class OldDesmanDocumentOperationAsEWalletDecrement : BaseOldDesmanDocumentOperation
    {
        public OldDesmanDocumentOperationAsEWalletDecrement()
            : base(WellknownDesmanDocumentOperations.EWalletDecrement)
        {
        }

        /// <summary>
        /// Сумма списания в копейках.
        /// </summary>
        public uint Amount { get; set; }

        /// <summary>
        /// Баланс смарт-карты после операции в копейках.
        /// </summary>
        public uint SmartCardBalance { get; set; }

        /// <summary>
        /// Id транзакции Nero.
        /// </summary>
        public long? NeroTransactionId { get; set; }

        /// <summary>
        /// Id сервера Nero.
        /// </summary>
        public Guid? NeroServerId { get; set; }
    }
}