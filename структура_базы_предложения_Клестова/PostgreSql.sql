/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     10.08.2018 17:12:09                          */
/*==============================================================*/


/*==============================================================*/
/* Table: DomainObjectIdentities                                */
/*==============================================================*/
create table DomainObjectIdentities (
   Id                   uuid                 not null,
   Base                 bigint               not null,
   Name                 varchar(1024)        not null,
   constraint PK_DOMAINOBJECTIDENTITIES primary key (Id)
);

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('18316568-8415-4B18-9C65-FAEA8CE65531', 100000000, 'Системный лог')
;

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('62A95954-A07E-4287-9E53-EA4DB0230CE3', 1, 'Владелец ключа')
;

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('B730BDEA-735F-44E3-B31C-B724511D7D49', 300000000, 'Карточка ключа')
;

/*==============================================================*/
/* Table: KeyOwners                                             */
/*==============================================================*/
create table KeyOwners (
   id                   INT8                 not null,
   createdate           TIMESTAMP            not null,
   owneridentity        uuid                 not null,
   constraint PK_KEYOWNERS primary key (id),
   constraint AK_OWNERIDENTITY_2_KEYOWNER unique (owneridentity)
);

/*==============================================================*/
/* Table: SystemLogs                                            */
/*==============================================================*/
create table SystemLogs (
   Id                   INT8                 not null,
   CreateDate           TIMESTAMP WITH TIME ZONE not null,
   Code                 INT4                 not null,
   Type                 INT4                 not null,
   Message              varchar(1024)        not null,
   Data                 TEXT                 not null,
   constraint PK_SYSTEMLOGS primary key (Id)
);

/*==============================================================*/
/* Table: SystemSettings                                        */
/*==============================================================*/
create table SystemSettings (
   Id                   uuid                 not null,
   Value                TEXT                 not null,
   Name                 varchar(1024)        not null,
   constraint PK_SYSTEMSETTINGS primary key (Id)
);

INSERT INTO SystemSettings (Id, Value, Name) VALUES('1A89A234-A3BF-411E-B975-3D1DC1084EEB', '00:01:00', 'Аварийный интервал ожидания инициализации реестра доменных объектов')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('568CB911-8B89-4572-A616-C870DBFD9D48', '00:00:00', 'Интервал ожидания исполнения SQL-команд')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('A98C4DB3-2ADD-4F4D-800D-F3B1AC08B8EF', '00:00:03', 'Аварийный интервал ожидания восстановления доменных объектов')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('8B0AEADA-5537-4B7B-ABEA-AB52DC751381', '1', 'Число потоков восстановления доменных объектов')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('D91FACE5-72C5-41FE-BD5C-A6606768BCF6', 'False', 'Отладочный режим')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('{CF0ED582-05EC-4AFE-883F-1D68A751B4A8}', '3E0D9F16-D99F-4E6C-B801-470AB0290EB1', 'Код продукта')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('1BF69D7C-2CBF-45a8-9526-0C33234ED62D', '@ProductVersion@', 'Версия продукта')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('76771569-AB89-4543-B210-E967FFC08CB5', '@ProductBuildVersion@', 'Версия сборки продукта')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('208B4E39-A3D1-45E2-A9DC-E79B3E064DF1', 'Хранилище уникальных ключей', 'Имя инсталляции')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('53377A05-2A4B-4317-9BC9-AC3F4688EE1C', md5(random()::text || clock_timestamp()::text)::uuid::VARCHAR(100), 'Код инсталляции')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('46737849-E535-4F3C-84FF-C273EB870365', '00:05:00', 'Интервал аккумуляции статистики')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('8A83E36E-94C1-41D8-9CAE-BAB8FAA56E3F', '00:15:00', 'Интервал существования в оперативной памяти данных отчётов созданных системными методами')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('0139DE9E-809E-4946-AA17-BC76790A1132', '<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<GlobalLoggerSettings IsDebugEnabled="False" IsInfoEnabled="False" IsWarnEnabled="False" IsErrorEnabled="False" IsFatalEnabled="False" IsVerboseEnabled="False"/>', 'Настройки расширенного логирования')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('8DCAACBF-8EA3-44A2-A954-B4F0875BA4D1', '<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<GlobalLoggerSettings IsDebugEnabled="True" IsInfoEnabled="False" IsWarnEnabled="True" IsErrorEnabled="True" IsFatalEnabled="True" IsVerboseEnabled="False"/>', 'Настройки  логирования')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('05EACD8D-CED0-4A9C-95DE-3695C415804D', '00:15:00', 'Таймаут закрытия временных периодов')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('18CD1858-EC96-486D-A004-4C576EB9E61E', '', 'Capacity колонок ключей по дням при старте системы')
;


INSERT INTO SystemSettings (Id, Value, Name) VALUES('7B6174C5-D085-46E8-AE93-20BE3CDF8D23', '00:00:01', 'Хранилище ключей в памяти - таймаут на получение доступа к ключам')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('1FE6E001-DC9F-4C18-B5A1-BE49B55FC35D', '0', 'Хранилище ключей в памяти - минимальное значение ShardIndex')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('59301388-695D-4D8E-A0E6-E5B38BCBD149', '255', 'Хранилище ключей в памяти - максмальное значение ShardIndex')
;

INSERT INTO SystemSettings (Id, Value, Name) VALUES('7DAC0857-B7C3-48F5-BDA6-55952CC9EE3A', '95', 'Количество хранимых дней с ключами в прошлое')
;


/*==============================================================*/
/* Table: UniqueKeys                                            */
/*==============================================================*/
create table UniqueKeys (
   Id                   INT8                 not null,
   CreateDate           TIMESTAMP            not null,
   Key                  uuid                 not null,
   OwnerKeyIdentity     INT8                 not null,
   OwnerId              INT8                 not null
)
PARTITION BY RANGE (CreateDate);

do
$$

DECLARE             daydate date;
DECLARE             daycount integer;

DECLARE tablename text;

DECLARE             command text;

BEGIN

tablename = 'UniqueKeys';
daydate = to_date('@StartDay@', 'YYYYMMDD');
daycount = @DayCount@;

FOR i IN 0..daycount LOOP

command =  'CREATE TABLE ' || tablename || to_char(daydate + i, 'YYYYMMDD') || ' PARTITION OF '|| tablename ||' FOR VALUES FROM ('''|| to_char(daydate+ i, 'YYYYMMDD') || ''') TO (''' || to_char(daydate + i + 1, 'YYYYMMDD') ||''')';
--raise notice '%',command;
EXECUTE command;

END LOOP;

END
$$;

