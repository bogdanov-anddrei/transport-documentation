

set quoted_identifier on
go



CREATE TABLE "EWallet_Increment"
( 
	"Key"                binary(16)  NOT NULL ,
	"Amount"             bigint  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL 
)
go



CREATE TABLE "EWallet_Link"
( 
	"Key"                binary(16)  NOT NULL ,
	"Amount"             bigint  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "EWallet_Transaction"
( 
	"Key"                binary(16)  NOT NULL ,
	"Type"               bit  NOT NULL ,
	"Amount"             bigint  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "TiketCombo_Buy"
( 
	"Key"                binary(16)  NOT NULL ,
	"Amount"             bigint  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"Type"               int  NOT NULL ,
	"Form"               int  NOT NULL ,
	"BeginDate"          smalldatetime  NOT NULL ,
	"EndDate"            smalldatetime  NOT NULL 
)
go



CREATE TABLE "TiketCombo_Link"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"Type"               int  NOT NULL ,
	"Form"               int  NOT NULL ,
	"BeginDate"          smalldatetime  NOT NULL ,
	"EndDate"            smalldatetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "TiketCombo_Use"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"Type"               int  NOT NULL ,
	"Form"               int  NOT NULL ,
	"BeginDate"          smalldatetime  NOT NULL ,
	"EndDate"            smalldatetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "TiketCommon_Buy"
( 
	"Key"                binary(16)  NOT NULL ,
	"Amount"             bigint  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"Type"               int  NOT NULL ,
	"BeginDate"          smalldatetime  NOT NULL ,
	"EndDate"            smalldatetime  NOT NULL 
)
go



CREATE TABLE "TiketCommon_Link"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"Type"               int  NOT NULL ,
	"BeginDate"          smalldatetime  NOT NULL ,
	"EndDate"            smalldatetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "TiketCommon_Use"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"Type"               int  NOT NULL ,
	"BeginDate"          smalldatetime  NOT NULL ,
	"EndDate"            smalldatetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "TiketLimited_Activation"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL ,
	"Types"              int  NOT NULL 
)
go



CREATE TABLE "TiketLimited_Buy"
( 
	"Key"                binary(16)  NOT NULL ,
	"Amount"             bigint  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"Type"               int  NOT NULL 
)
go



CREATE TABLE "TiketLimited_Link"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"ServerId"           bigint  NOT NULL ,
	"ClientId"           bigint  NOT NULL ,
	"Type"               int  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL 
)
go



CREATE TABLE "TiketLimited_Use"
( 
	"Key"                binary(16)  NOT NULL ,
	"IssuerCreateDate"   datetime  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL ,
	"Type"               int  NOT NULL 
)
go


