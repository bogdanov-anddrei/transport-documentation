

set quoted_identifier on
go



CREATE TABLE "ConductorCode"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Name"               nvarchar(256)  NOT NULL 
)
go



ALTER TABLE "ConductorCode"
	ADD CONSTRAINT "XPKConductorCode" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "Device"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"OrganizationId"     bigint  NOT NULL ,
	"IsBlocked"          bit  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL ,
	"ExpirationDate"     datetime2  NULL 
)
go



ALTER TABLE "Device"
	ADD CONSTRAINT "XPKDevice" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "DeviceCommunication"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"IsBlocked"          bit  NOT NULL ,
	"CertificateDate"    varbinary(max)  NOT NULL ,
	"ExpirationDate"     datetime2  NULL ,
	"DeviceId"           bigint  NOT NULL 
)
go



ALTER TABLE "DeviceCommunication"
	ADD CONSTRAINT "XPKDeviceCommunication" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "DeviceDocument"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"IsBlocked"          bit  NOT NULL ,
	"CertificateDate"    varbinary(max)  NOT NULL ,
	"ExpirationDate"     datetime2  NULL ,
	"DeviceId"           bigint  NOT NULL ,
	"Permissions"        bigint  NOT NULL 
)
go



ALTER TABLE "DeviceDocument"
	ADD CONSTRAINT "XPKDeviceDocument" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "GarageCode"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Name"               nvarchar(256)  NOT NULL 
)
go



ALTER TABLE "GarageCode"
	ADD CONSTRAINT "XPKGarageCode" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "Organization"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"IsBlocked"          bit  NOT NULL ,
	"ExpirationDate"     datetime2  NULL 
)
go



ALTER TABLE "Organization"
	ADD CONSTRAINT "XPKOrganization" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "RouteCode"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Name"               nvarchar(256)  NOT NULL 
)
go



ALTER TABLE "RouteCode"
	ADD CONSTRAINT "XPKRouteCode" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "StopCode"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Name"               nvarchar(256)  NOT NULL 
)
go



ALTER TABLE "StopCode"
	ADD CONSTRAINT "XPKStopCode" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "ZoneCode"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Name"               nvarchar(256)  NOT NULL 
)
go



ALTER TABLE "ZoneCode"
	ADD CONSTRAINT "XPKZoneCode" PRIMARY KEY  CLUSTERED ("Id" ASC)
go




ALTER TABLE "Device"
	ADD CONSTRAINT "R_471" FOREIGN KEY ("OrganizationId") REFERENCES "Organization"("Id")
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE "DeviceCommunication"
	ADD CONSTRAINT "R_472" FOREIGN KEY ("DeviceId") REFERENCES "Device"("Id")
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE "DeviceDocument"
	ADD CONSTRAINT "R_473" FOREIGN KEY ("DeviceId") REFERENCES "Device"("Id")
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


