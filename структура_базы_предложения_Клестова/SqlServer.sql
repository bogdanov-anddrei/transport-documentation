

set quoted_identifier on
go



DECLARE @___DB nvarchar(1024)
DECLARE @___SQL nvarchar(2048)

SELECT @___DB = DB_NAME()

SET @___SQL = 'ALTER DATABASE ' + @___DB + ' COLLATE Cyrillic_General_CI_AS'

EXECUTE sp_executesql @___SQL

GO





/*

GO 
-- ��� ������� ������� ������������� �������  ��������� �������� ������ ��� ������  � ������������� ����� � ������� �����������������

DECLARE
	@DatabaseName nvarchar(4000) = DB_NAME(),
	@DafaultDataDir nvarchar(4000) =   CONVERT( nvarchar(4000), serverproperty('InstanceDefaultDataPath'));
	
		
DECLARE @FileGroupCount int = 24; -- ���������� �������� �����
DECLARE @FileGroupPrefix nvarchar(50) = 'FG';
DECLARE @StartDate datetime = '20180101'; -- ��������� ����


DECLARE @counter int = 0, @FilGroupName nvarchar(1000), @command nvarchar(max),  @DayIdFunctionValues nvarchar(max) ='', @FileGroupValues nvarchar(max) ='';

WHILE @counter < @FileGroupCount
BEGIN  
 SET @FilGroupName = @FileGroupPrefix + '-' + CONVERT(nvarchar(7), DATEADD(month,@counter,@StartDate), 120)
 
 SET @command =  'ALTER DATABASE '+ @DatabaseName +' ADD FILEGROUP ['+ @FilGroupName +'];'      
 EXEC (@Command)
   
 SET @command = 'ALTER DATABASE '+ @DatabaseName +' ADD FILE ( NAME = N'''+ @FilGroupName +'-File'', FILENAME = N''' + @DafaultDataDir+ @DatabaseName + '_'+ @FilGroupName +'-File.ndf'' , SIZE = 8192KB , FILEGROWTH = 65536KB ) TO FILEGROUP ['+ @FilGroupName +']'
 EXEC (@Command)  

 SET @DayIdFunctionValues = @DayIdFunctionValues + '''' + CONVERT(nvarchar(10), DATEADD(month,@counter,@StartDate), 120) + ''''+ ', '; 
 SET @FileGroupValues = @FileGroupValues + '['+ @FilGroupName + '], ';
      
 SET @counter = @counter + 1;   
END;

SET @command = 'CREATE PARTITION FUNCTION [CreateDateFunction](datetime2) AS RANGE RIGHT FOR VALUES (' + LEFT(@DayIdFunctionValues, LEN(@DayIdFunctionValues) -1) + ')';
EXEC (@command)

SET @command = 'CREATE PARTITION SCHEME [CreateDateScheme] AS PARTITION [CreateDateFunction] TO ([PRIMARY],' + LEFT(@FileGroupValues, LEN(@FileGroupValues) -1) + ')';
EXEC (@command)

GO

*/

--  ��� ��������� �������


CREATE PARTITION FUNCTION [CreateDateFunction](datetime2) AS RANGE RIGHT FOR VALUES ('2018-01-01', '2018-02-01', '2018-03-01', '2018-04-01', '2018-05-01', '2018-06-01', '2018-07-01', '2018-08-01', '2018-09-01', '2018-10-01', '2018-11-01', '2018-12-01')
GO

CREATE PARTITION SCHEME [CreateDateScheme] AS PARTITION [CreateDateFunction] TO ([PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY],[PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY])
GO





CREATE TABLE "BlackSmartCardBlocks"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Revision"           bigint  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Available"          bit  NOT NULL ,
	"Data"               image  NOT NULL 
)
go



ALTER TABLE "BlackSmartCardBlocks"
	ADD CONSTRAINT "XPKBlackSmartCardBlocks" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "DomainObjectIdentities"
( 
	"Id"                 uniqueidentifier  NOT NULL ,
	"Base"               bigint  NOT NULL ,
	"Name"               nvarchar(1024)  NOT NULL 
)
go



ALTER TABLE "DomainObjectIdentities"
	ADD CONSTRAINT "XPKDomainObjectIdentities" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "IncomingDocumentLogs"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Code"               int  NOT NULL ,
	"Message"            nvarchar(1024)  NOT NULL ,
	"Data"               ntext  NOT NULL ,
	"Type"               int  NOT NULL ,
	"IncomingDocumentId" bigint  NOT NULL 
)
ON CreateDateScheme( CreateDate )
go



ALTER TABLE "IncomingDocumentLogs"
	ADD CONSTRAINT "XPKIncomingDocumentLogs" PRIMARY KEY  CLUSTERED ("Id" ASC,"CreateDate" ASC)
	WITH
	(
		IGNORE_DUP_KEY = ON
	)
	 ON CreateDateScheme( CreateDate )
go



CREATE TABLE "IncomingDocuments"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Context"            ntext  NOT NULL ,
	"Document"           image  NOT NULL ,
	"State"              tinyint  NOT NULL ,
	"Revision"           bigint  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"DocumentHash"       varbinary(32)  NOT NULL ,
	"ExternalId"         bigint  NOT NULL ,
	"StateDetails"       tinyint  NOT NULL 
)
go



ALTER TABLE "IncomingDocuments"
	ADD CONSTRAINT "XPKIncomingDocuments" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



ALTER TABLE "IncomingDocuments"
	ADD CONSTRAINT "XAK1IncomingDocuments" UNIQUE ("DocumentHash"  ASC)
go



CREATE TABLE "NeroAccountLogs"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Code"               int  NOT NULL ,
	"Message"            nvarchar(1024)  NOT NULL ,
	"Data"               ntext  NOT NULL ,
	"Type"               int  NOT NULL ,
	"NeroAccountId"      bigint  NOT NULL 
)
ON CreateDateScheme( CreateDate )
go



ALTER TABLE "NeroAccountLogs"
	ADD CONSTRAINT "XPKNeroAccountLogs" PRIMARY KEY  CLUSTERED ("Id" ASC,"CreateDate" ASC)
	WITH
	(
		IGNORE_DUP_KEY = ON
	)
	 ON CreateDateScheme( CreateDate )
go



CREATE TABLE "NeroAccounts"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Revision"           bigint  NOT NULL ,
	"IsBlocked"          bit  NOT NULL ,
	"NeroLastEventId"    bigint  NULL ,
	"State"              tinyint  NOT NULL ,
	"SmartCardId"        bigint  NOT NULL 
)
go



ALTER TABLE "NeroAccounts"
	ADD CONSTRAINT "XPKNeroAccounts" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "SmartCardBlackStateChanges"
( 
	"Id"                 bigint  NOT NULL ,
	"Available"          bit  NOT NULL ,
	"ChronologicalId"    bigint IDENTITY ( 1,1 ) ,
	"IsBlack"            bit  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"SmartCardId"        bigint  NOT NULL 
)
ON CreateDateScheme( CreateDate )
go



ALTER TABLE "SmartCardBlackStateChanges"
	ADD CONSTRAINT "XPKSmartCardBlackStateChanges" PRIMARY KEY  CLUSTERED ("Id" ASC,"CreateDate" ASC)
	 ON CreateDateScheme( CreateDate )
go



CREATE TABLE "SmartCardLogs"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Code"               int  NOT NULL ,
	"Message"            nvarchar(1024)  NOT NULL ,
	"Data"               ntext  NOT NULL ,
	"Type"               int  NOT NULL ,
	"SmartCardId"        bigint  NOT NULL 
)
ON CreateDateScheme( CreateDate )
go



ALTER TABLE "SmartCardLogs"
	ADD CONSTRAINT "XPKSmartCardLogs" PRIMARY KEY  CLUSTERED ("Id" ASC,"CreateDate" ASC)
	WITH
	(
		IGNORE_DUP_KEY = ON
	)
	 ON CreateDateScheme( CreateDate )
go



CREATE TABLE "SmartCards"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Revision"           bigint  NOT NULL ,
	"NativeId"           varbinary(255)  NOT NULL ,
	"IncomingDocumentId" bigint  NOT NULL ,
	"SerialNumber"       bigint  NOT NULL ,
	"OrderNumber"        bigint  NOT NULL ,
	"IsBlocked"          bit  NOT NULL ,
	"State"              tinyint  NOT NULL ,
	"OfflineAccountId"   bigint  NOT NULL ,
	"OnlineAccountId"    bigint  NOT NULL ,
	"IssuerType"         varchar(255)  NOT NULL ,
	"Signature"          varbinary(255)  NOT NULL ,
	"IssueDate"          datetime2  NOT NULL ,
	"BlockLastEventId"   bigint  NULL ,
	"IsActive"           bit  NOT NULL 
)
go



ALTER TABLE "SmartCards"
	ADD CONSTRAINT "XPKSmartCards" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



ALTER TABLE "SmartCards"
	ADD CONSTRAINT "XAK1SmartCards" UNIQUE ("NativeId"  ASC)
go



CREATE TABLE "SystemLogs"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Code"               int  NOT NULL ,
	"Message"            nvarchar(1024)  NOT NULL ,
	"Data"               ntext  NOT NULL ,
	"Type"               int  NOT NULL 
)
ON CreateDateScheme( CreateDate )
go



ALTER TABLE "SystemLogs"
	ADD CONSTRAINT "XPKSystemLogs" PRIMARY KEY  CLUSTERED ("Id" ASC,"CreateDate" ASC)
	WITH
	(
		IGNORE_DUP_KEY = ON
	)
	 ON CreateDateScheme( CreateDate )
go



CREATE TABLE "SystemSettings"
( 
	"Id"                 uniqueidentifier  NOT NULL ,
	"Value"              ntext  NOT NULL ,
	"Name"               nvarchar(1024)  NOT NULL 
)
go



ALTER TABLE "SystemSettings"
	ADD CONSTRAINT "XPKSystemSettings" PRIMARY KEY  CLUSTERED ("Id" ASC)
go



CREATE TABLE "TaskLogs"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Code"               int  NOT NULL ,
	"Message"            nvarchar(1024)  NOT NULL ,
	"Data"               ntext  NOT NULL ,
	"Type"               int  NOT NULL ,
	"TaskId"             bigint  NOT NULL 
)
ON CreateDateScheme( CreateDate )
go



ALTER TABLE "TaskLogs"
	ADD CONSTRAINT "XPKTaskLogs" PRIMARY KEY  CLUSTERED ("Id" ASC,"CreateDate" ASC)
	WITH
	(
		IGNORE_DUP_KEY = ON
	)
	 ON CreateDateScheme( CreateDate )
go



CREATE TABLE "Tasks"
( 
	"Id"                 bigint  NOT NULL ,
	"CreateDate"         datetime2  NOT NULL ,
	"Revision"           bigint  NOT NULL ,
	"ModificationDate"   datetime2  NOT NULL ,
	"Available"          bit  NOT NULL ,
	"Parameters"         image  NOT NULL 
)
go



ALTER TABLE "Tasks"
	ADD CONSTRAINT "XPKTasks" PRIMARY KEY  CLUSTERED ("Id" ASC)
go




ALTER TABLE "IncomingDocumentLogs"
	ADD CONSTRAINT "R_464" FOREIGN KEY ("IncomingDocumentId") REFERENCES "IncomingDocuments"("Id")
		ON DELETE CASCADE
		ON UPDATE NO ACTION
go




ALTER TABLE "NeroAccountLogs"
	ADD CONSTRAINT "R_469" FOREIGN KEY ("NeroAccountId") REFERENCES "NeroAccounts"("Id")
		ON DELETE CASCADE
		ON UPDATE NO ACTION
go




ALTER TABLE "SmartCardBlackStateChanges"
	ADD CONSTRAINT "R_470" FOREIGN KEY ("SmartCardId") REFERENCES "SmartCards"("Id")
		ON DELETE CASCADE
		ON UPDATE NO ACTION
go




ALTER TABLE "SmartCardLogs"
	ADD CONSTRAINT "R_465" FOREIGN KEY ("SmartCardId") REFERENCES "SmartCards"("Id")
		ON DELETE CASCADE
		ON UPDATE NO ACTION
go




ALTER TABLE "SmartCards"
	ADD CONSTRAINT "R_467" FOREIGN KEY ("OfflineAccountId") REFERENCES "NeroAccounts"("Id")
		ON UPDATE NO ACTION
go




ALTER TABLE "SmartCards"
	ADD CONSTRAINT "R_468" FOREIGN KEY ("OnlineAccountId") REFERENCES "NeroAccounts"("Id")
		ON UPDATE NO ACTION
go




ALTER TABLE "TaskLogs"
	ADD CONSTRAINT "R_463" FOREIGN KEY ("TaskId") REFERENCES "Tasks"("Id")
		ON DELETE CASCADE
		ON UPDATE NO ACTION
go



INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('966FD893-94DC-4B25-BD79-8735790F3DEF', 
2300000000, '��������� ���')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('2B82FB09-6000-42BE-A0A0-E6E400F400BD', 
2400000000, '��� ���������� ������')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('CDF96B63-25AC-466C-8C79-33422614FB24', 
2500000000, '������')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('09485FD2-CED5-4675-903D-30E3E64C9A78', 
2600000000, '��� ���������� ����� ������')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('120E63BA-EC23-4889-A4E7-92C9CD27CE14', 
2700000000, '���� �������')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('6937CB1A-453A-4C59-984D-83032F5AFB1E', 
2800000000, '��� ��������� �����-�����')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('B1B03B07-49BE-4397-9D37-EF9FA68C53BB', 
2900000000, '�����-�����')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('787D804C-6CF1-4428-9685-6CD97BDFC815', 
3000000000, '��� ��������� ����� Nero')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('BF3EF7E4-C76F-460B-8EF1-D61C08C0BF0D', 
3100000000, '���� Nero')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('59320659-3FA9-483B-B878-6BA20A91FF58', 
3200000000, '��������� ���������� �����-�����')
GO

INSERT INTO DomainObjectIdentities (Id, Base, Name) VALUES('7B7F8BEF-9BA0-478C-B63E-8477E0251720', 
3300000000, '����-���� �� ������������ �����-����')
GO





INSERT INTO SystemSettings (Id, Value, Name) VALUES('CF0ED582-05EC-4AFE-883F-1D68A751B4A8', 'AF9AF5DF-3140-4217-B264-4DD7F8613BB9', '��� ��������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('1BF69D7C-2CBF-45a8-9526-0C33234ED62D', '@ProductVersion@', '������ ��������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('76771569-AB89-4543-B210-E967FFC08CB5', '@ProductBuildVersion@', '������ ������ ��������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('208B4E39-A3D1-45E2-A9DC-E79B3E064DF1', '��������� �����-����', '��� �����������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('53377A05-2A4B-4317-9BC9-AC3F4688EE1C', CAST(NEWID() AS VARCHAR(100)), '��� �����������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('0139DE9E-809E-4946-AA17-BC76790A1132', '<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<GlobalLoggerSettings IsDebugEnabled="False" IsInfoEnabled="False" IsWarnEnabled="True" IsErrorEnabled="True" IsFatalEnabled="True" IsVerboseEnabled="False"/>', '��������� ������������ �����������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('8DCAACBF-8EA3-44A2-A954-B4F0875BA4D1', '<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<GlobalLoggerSettings IsDebugEnabled="True" IsInfoEnabled="False" IsWarnEnabled="True" IsErrorEnabled="True" IsFatalEnabled="True" IsVerboseEnabled="False"/>', '���������  �����������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('1A89A234-A3BF-411E-B975-3D1DC1084EEB', '00:01:00', '��������� �������� �������� ������������� ������� �������� ��������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('A98C4DB3-2ADD-4F4D-800D-F3B1AC08B8EF', '00:00:03', '��������� �������� �������� �������������� �������� ��������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('12C764BF-F0E5-4166-AF39-87BB5E4C2FDD', '1', '����� ������� �������������� �������� ��������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('79A65AD4-BBB4-4FB8-976C-8F930C3A0503', '00:00:00', '�������� �������� ���������� SQL-������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('D91FACE5-72C5-41FE-BD5C-A6606768BCF6', 'False', '���������� �����')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('32604CEE-FA5E-4D81-8740-6EF2B5FF5519', '', '������������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('46737849-E535-4F3C-84FF-C273EB870365', '00:15:00', '�������� ����������� ����������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('B3D1BB73-9640-4275-AC28-997AFA8DC10E', '00:10:00', '�������� ������������� � ����������� ������ ���������� ������������ ������ � ����������� ����')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('620464CE-996C-44D5-90CD-A920CDBA8D91', '00:00:10', '������� �������� ��������� ���������� �� ��������� ��������� �����-�����')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('63BB9EC3-0F3F-4DE8-94B7-853E25157A12', '12:00:10', '������ �����-����: �������� ������� ���������� ���������� � ���������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('AF3A822B-EF40-499F-AC7F-EEFE1810CC92', '00:03:00', '������ �����-����: �������� ������������ ���������� � ������ �����-������� � ���������')
GO

INSERT INTO SystemSettings (Id, Value, Name) VALUES('1EC45E85-BB5A-4E1B-BD39-9186BE654FF8', '00:30:33', '������ �����-����: �������� �������� � �������������� ���������� � ���������')
GO





CREATE SEQUENCE SmartCardBlackStateChageChronologicalId AS bigint START WITH 1 INCREMENT BY 1 CYCLE NO CACHE
GO



